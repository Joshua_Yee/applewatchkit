import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StateService {
//BLE parameters
BLE_heartReate$ = new BehaviorSubject<number>(0);
BLE_cadence$ = new BehaviorSubject<number>(0);
BLE_temperature$ = new BehaviorSubject<number>(0);

//GEO parameters
GEO_coordSet$ = new BehaviorSubject<{prev:any, current:any}>({prev:null, current:null});
GEO_distance$ = new BehaviorSubject<number>(0);
GEO_totalDistance$ = new BehaviorSubject<number>(0);
GEO_speed$ = new BehaviorSubject<number>(0);

//Activity
activity_counter$ = new BehaviorSubject<number>(0);
activity_cadence$ = new BehaviorSubject<number>(0); //override by BLE_cadence
activity_count$ = new BehaviorSubject<number>(0); // counts by AI-motion
activity_calorie$ = new BehaviorSubject<number>(0); // calculated by cal algorithm


//Availability
isAvailble_cadence$ = new BehaviorSubject<boolean>(false);
isAvailble_heartRate$ = new BehaviorSubject<boolean>(false);
isAvailble_GPS$ = new BehaviorSubject<boolean>(false);
isAvailble_temperature$ = new BehaviorSubject<boolean>(false);


  constructor() { }
}
