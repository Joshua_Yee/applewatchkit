import { Component } from '@angular/core';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { StateService } from '../state.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  heartRate;
  cadence;
  duration;
  speed;
  totalDistance;
  calories;
  count;
  
  constructor(private state: StateService) {
    this.recordActivity()
      .subscribe(params=>{
      
      this.heartRate = params.heartRate.pretty
      this.cadence = params.cadence.pretty
      this.duration = params.time.pretty
      this.speed = params.speed.pretty
      this.totalDistance = params.distance.pretty
      this.calories = params.calorie.pretty
    })
};

  recordActivity(){   
    const startTime = Math.floor(new Date().getTime()/1000);
    setInterval(()=>{
      let newTime = Math.floor(new Date().getTime()/1000);
      this.state.activity_counter$.next(newTime-startTime);
    },1000);

  
    return combineLatest([
        this.state.BLE_heartReate$,
        this.state.BLE_cadence$,
        this.state.activity_counter$,
        this.state.activity_calorie$,
        this.state.GEO_speed$,
        this.state.GEO_totalDistance$,
    ]).pipe(
      map(params=>{
          return ( {
            heartRate: {
              value:params[0],
              unit:"bpm",
              pretty:(params[0]??'--')+" bpm",
            },
            cadence: {
              value:params[1],
              unit:"spm",
              pretty:(params[1]??'--')+" spm",
            },
            time: {
              value:params[2],
              unit:"sec",
              pretty:`${Number(params[2]) ? Math.floor(Number(params[2])/60)+'"'+Number(params[2])%60+"'": `0"00'`}`,
            },
            calorie: {
              value:params[3],
              unit:"kCal",
              pretty:(params[3]??'--')+" kCal",
            },
            speed: {
              value:params[4],
              unit:"km/h",
              pretty:Number(params[4]).toFixed(2)+" km/h"??'--'+" km/h",
            },
            distance: {
              value:params[5],
              unit:"km",
              pretty:Number(params[5]).toFixed(2)+" km"??'--'+" km",
            },
          })
        })
    )
    
  }

}
